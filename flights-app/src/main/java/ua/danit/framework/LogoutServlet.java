package ua.danit.framework;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    setCookieAge0(req, resp);
    resp.sendRedirect("/");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    setCookieAge0(req, resp);
    resp.sendRedirect("/");
  }

  private void setCookieAge0(HttpServletRequest req, HttpServletResponse resp) {
    Cookie[] cookies = req.getCookies();
    for (Cookie cookie : cookies) {
      String cookieName = cookie.getName();
      if (cookieName != null&&cookieName.equals("flight-user")) {
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
      }
    }
  }
}
