package ua.danit.application.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ROUTES")
public class Route {

  String airline;	// 2-letter (IATA) or 3-letter (ICAO) code of the airline.
  Integer airlineId;	//Unique OpenFlights identifier for airline (see Airline).
  String sourceAirport;	//3-letter (IATA) or 4-letter (ICAO) code of the source airport.
  Integer sourceAirportId; //ID	Unique OpenFlights identifier for source airport (see Airport)
  String destinationAirport;	//3-letter (IATA) or 4-letter (ICAO) code of the destination airport.
  Integer destinationAirportId;	//Unique OpenFlights identifier for destination airport (see Airport)
  Boolean codeshare;	//"Y" if this flight is a codeshare (that is, not operated by Airline, but another carrier), empty otherwise.
  Integer stops;	//Number of stops on this flight ("0" for direct)
  String equipment;	//3-letter codes for plane type(s) generally used on this flight, separated by spaces

  /**
   Sample entries

   BA,1355,SIN,3316,LHR,507,,0,744 777
   BA,1355,SIN,3316,MEL,3339,Y,0,744
   TOM,5013,ACE,1055,BFS,465,,0,320
   */
}
